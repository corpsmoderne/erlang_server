-module(servworker).
-include_lib("eunit/include/eunit.hrl").
-export([do_recv/4, net_send/2, loop_ping/1]).

net_send(Sock, Msg) ->
    Ret = gen_tcp:send(Sock, Msg),
    case Ret of
	ok -> ok;
	{error, Val} -> io:fwrite("~p :: Error: ~p~n", [Sock, Val])
    end,
    Ret.

do_recv(continue, Parent, Sock, Name) ->
    case gen_tcp:recv(Sock, 0) of
        {ok, B} ->
	    Line = string:tokens(binary_to_list(B), "\r\n"),
	    case Line of
		[] -> Next = continue, NextName = Name;
		[L] -> 
		    Toks = string:tokens(L, " "),
		    {Next, NextName} = handle_message(Parent, Sock, Name, Toks)
	    end;
        {error, closed} ->
	    Next = stop, NextName = Name
    end,
    do_recv(Next, Parent, Sock, NextName);
do_recv(stop, Parent, Sock, Name) ->
    io:fwrite("~p ~s:: disconnected~n", [Sock, Name]),
    gen_tcp:close(Sock),
    Parent ! { close, Sock }.

handle_message(Parent, Sock, _Name, ["/nick", Nick]) ->
    Parent ! { changeNick, Sock, Nick }, 
    { continue, Nick };
handle_message(_Parent, Sock, Name, ["/ping"]) ->
    Pid = spawn(?MODULE, loop_ping, [Sock]),
    io:fwrite("Starting Ping process: ~p~n", [Pid]),
    { continue, Name };
handle_message(_Parent, _Sock, Name, ["/quit"]) ->
    { stop, Name };
handle_message(Parent, Sock, Name, ["/list"]) ->
    Parent ! { listUsers, Sock },
    { continue, Name };
handle_message(Parent, Sock, Name, ["/me" | Msg]) ->
    check_anon(Name, 
	       fun() -> send_me(Parent, Name, Msg) end,
	       fun(Err) -> net_send(Sock, Err), {continue,Name } end);
handle_message(Parent, Sock, Name, Msg) ->
    check_anon(Name, 
	       fun() -> send_msg(Parent, Name, Msg) end,
	       fun(Err) -> net_send(Sock, Err), {continue,Name } end).

check_anon(Name, FctOk, FctErr) ->
    case string:slice(Name, 0, 4) of
	"Anon" ->
	    FctErr(io_lib:format("*** Change your nickname first with "
				 "/nick <your name>~n", []));
	_ -> FctOk()
    end.

send_me(Parent, Name, Msg) ->
    Parent ! { sendAll, 
	       io_lib:format("* ~s ~s~n", [Name, string:join(Msg," ")]) },
    { continue, Name }.

send_msg(Parent, Name, Msg) ->
    Parent ! { sendAll, 
	       io_lib:format("[~s] ~s~n", [Name,string:join(Msg," ")])},
    { continue, Name }.

loop_ping(Sock) ->
    receive
	stop -> ok
    after 
	1000 ->
	    {Y,M,D} = date(),
	    {H,Min,S} = time(),
	    Ret = net_send(Sock, io_lib:format("Ping! ~B/~B/~B ~B:~B:~B~n", 
					       [Y,M,D,H,Min,S])),
	    case Ret of
		ok -> loop_ping(Sock);
		{error, _} -> io:fwrite("Error, stoping ping process: ~p~n",
				       [self()])
	    end
    end.

% handle_message(_Parent, Sock, Name, [[$/|Cmd]|_]) ->
%     net_send(Sock, 
%	     io_lib:format("*** Error: command /~s unknown "
%			   "or wrong arguments.~n", [Cmd])),
%    {continue, Name};

%%
%% Unit tests
%%

send_me_test() ->
    { Ret, _ } = send_me(self(), "Toto", ["is", "happy"]),
    ?assertEqual(Ret, continue),
    receive
	{ sendAll, Msg } ->
	    ?assert(string:equal(Msg, "* Toto is happy\n"))
    after
	100 ->
	    ?assert("Timed out")
    end.

send_msg_test() ->
    { Ret, _ } = send_msg(self(), "Toto", ["hello", "world"]),
    ?assertEqual(Ret, continue),
    receive
	{ sendAll, Msg } ->
	    ?assert(string:equal(Msg,"[Toto] hello world\n"))
    after
	100 ->
	    ?assert("Timed out")
    end.

check_anon_ok_test() ->
    ?assert(check_anon("Toto", fun() -> true end, fun(_) -> false end)).
check_anon_notok_test() ->
    ErrMsg = "*** Change your nickname first with /nick <your name>\n",
    ?assert(check_anon("Anon0", 
		       fun() -> false end, 
		       fun(Err) -> string:equal(Err, ErrMsg) end)).


