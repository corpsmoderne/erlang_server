#!/bin/sh

MODULE=route

if [ "$1" != "" ]; then
   MODULE=$1
fi

erl << EOF
cover:start().
cover:compile_module($MODULE).
eunit:test($MODULE,[verbose]).
cover:analyse_to_file($MODULE).
cover:analyse_to_file($MODULE,[html]).
EOF

