-module(route).
-include_lib("eunit/include/eunit.hrl").
-export([route/3]).

route([], _Req, _Log) ->
    {200, "text/plain", "Hello world\n"};
route(["time"], _Req, _Log) ->
    {_Date,{H,M,S}} = calendar:local_time(),
    {200, "text/plain", io_lib:format("Time: ~B:~B:~B~n", [H,M,S])};
route(["pub"|Path], _Req, _Log) ->
    Ret = file:read_file(string:join(["pub"]++Path, "/")),
    Res = case Ret of
	      {ok, Data} -> {200, get_type(Path), Data};
	      {error,enoent} -> {404, "text/plain", "404 Not Found\n"};
	      {error,eaccess} -> {403, "text/plain", "403 Forbidden\n"};
	      {error,eisdir} -> {403, "text/plain", 
				 "Directory listing forbidden\n"};
	      {E,V} -> {500, "text/plain", 
			io_lib:format("??? ~p ~p~n", [E,V])}
	  end,
    Res;
route(["crash"], _Req, Log) ->
    Log("About to crash!~n", []),
    toto = tata;
route(Path, _Req, _Log) ->
    {404, "text/plain", io_lib:format("Unknown path: ~p~n", [Path])}.

get_type(Path) ->
    Ext = lists:last(string:tokens(lists:last(Path), ".")),
    mime_type(Ext).

mime_type("html") -> "text/html";
mime_type("png") ->  "image/png";
mime_type("jpg") -> "image/jpeg";
mime_type("jpeg") -> "image/jpeg";
mime_type("txt") -> "text/plain";
mime_type(_) -> "application/octet-stream".

%%
%% Unit tests
%%

route_home_test() ->
    {Code, MimeType, Content} = route([], #{}, fun(_,_) -> ok end),
    ?assertEqual(200, Code),
    ?assertEqual("text/plain", MimeType),
    ?assertEqual("Hello world\n", Content).

route_time_test() ->
    {Code, MimeType, Content} = route(["time"], #{}, fun(_,_) -> ok end),
    ?assertEqual(200, Code),
    ?assertEqual("text/plain", MimeType),
    [Txt,_Time] = string:tokens(Content, " "),
    ?assertEqual("Time:", Txt).

route_file_ok_test() ->
    {Code,MimeType, Content} = route(["pub","index.html"], 
				     #{}, fun(_,_) -> ok end),
    ?assertEqual(200, Code),
    ?assertEqual(MimeType, "text/html"),
    ?assert(string:length(Content) > 0).

route_file_not_found_test() ->
    {Code,MimeType, _Content} = route(["pub","no_file"], 
				     #{}, fun(_,_) -> ok end),
    ?assertEqual(404, Code),
    ?assertEqual(MimeType, "text/plain").
route_file_isdir_test() ->
    {Code,MimeType, _Content} = route(["pub"], 
				      #{}, fun(_,_) -> ok end),
    ?assertEqual(403, Code),
    ?assertEqual(MimeType, "text/plain").

route_unknown_path_test() ->
    {Code, MimeType, Content} = route(["dont_exist"], #{}, fun(_,_) -> ok end),
    ?assertEqual(404, Code),
    ?assertEqual("text/plain", MimeType),
    ?assertEqual(<<"Unknown path: [\"dont_exist\"]\n">>,
		 erlang:iolist_to_binary(Content)).

get_type_test() ->
    ?assertEqual("text/html", get_type(["pub","index.html"])),
    ?assertEqual("image/png", get_type(["pub","picture.png"])),
    ?assertEqual("image/jpeg", get_type(["pub","picture.jpg"])),
    ?assertEqual("image/jpeg", get_type(["pub","picture.jpeg"])),
    ?assertEqual("text/plain", get_type(["/pub","article.txt"])),
    ?assertEqual("application/octet-stream", get_type(["nothing","match"])).
