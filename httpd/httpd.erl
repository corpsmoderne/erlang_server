-module(httpd).
-include_lib("eunit/include/eunit.hrl").
-export([run/0, run/1, server/1, server_loop/4, do_recv/3]).
-import(route, [route/3]).

run() -> run(8080).
run(Port) -> spawn(?MODULE, server, [Port]).

server(Port) ->
    Log = get_logger(),
    {ok, LSock} = gen_tcp:listen(Port, [binary, {packet, http},
                                        {active, false}, {reuseaddr, true}]),
    Log("Listening on port ~B~n", [Port]),
    Looper = spawn(?MODULE, server_loop, [continue, self(), LSock, Log]),
    event_loop(Log),
    exit(Looper, kill),
    gen_tcp:close(LSock).

server_loop(stop, _, _, _) -> ok;
server_loop(continue, Parent, LSock, Log) ->
    case gen_tcp:accept(LSock) of
	{ ok, Sock } ->
	    spawn(?MODULE, do_recv, [ Sock, #{}, Log ]),
	    Next = continue;
	{ Ret, Sock } ->
	    Log("Erf? [~p] :: ~p~n", [Ret, Sock]),
	    Next = stop
    end,
    server_loop(Next, Parent, LSock, Log).

event_loop(Log) ->
    receive
	stop -> shutdown(Log)
    end.

shutdown(Log) ->
    Log("Shutting down...~n").

do_recv(Sock, stop, _Log) ->
    gen_tcp:close(Sock);
do_recv(Sock, Req, Log) ->
    case gen_tcp:recv(Sock, 0, 10000) of
	{ok, http_eoh} ->
	    process_req(Sock, Req, Log),
	    NewReq = stop;
        {ok, B} ->
	    NewReq = parse_buffer(Req, B, Log);
	{error, Cause} ->
	    Log("~p :: Socket closed: ~p~n", [Sock, Cause]),
	    NewReq = stop
    end,
    do_recv(Sock, NewReq, Log).

parse_buffer(Req, B, Log) ->
    Log(">>> ~p~n", [B]),
    case B of
	{http_request,Method,{abs_path,Path},{1,_}} ->
	    Req#{ method => Method, abs_path => Path};
	{http_header,_,Key,_,Value} ->
	    Req#{ Key => Value };
	{http_error,_} ->
	    stop
    end.

process_req(Sock, Req, Log) ->
    Log("!!! ~p~n", [Req]),
    Tail = string:tokens(maps:get(abs_path, Req), "/"),
    try 
	{Code,MimeType,Content} = route(Tail, Req, Log),
	send_response(Sock, Code, MimeType, Content)
    of
	_ -> ok
    catch
	Exception:Reason -> Log("Crashed: ~p~n",
				[{caught, Exception, Reason}]),
			    send_response(Sock, 500, "text/plain",
					  "Internal Server Error\n")
    end.

send_response(Sock, Status, Type, Content) ->
    gen_tcp:send(Sock, headers(Status, Type, 
			       byte_size(iolist_to_binary(Content)))),
    gen_tcp:send(Sock, Content).

headers(Status, Type, Size) ->
    io_lib:format("HTTP/1.1 ~B ~s~n"
		  "Server: MyErlangServer~n"
		  "Content-Length: ~B~n"
		  "Content-Type: ~s~n~n"
		 , [Status, status(Status), Size, Type]).

status(200) -> "OK";
status(403) -> "Forbidden";
status(404) -> "Not Found";
status(500) -> "Internal Server Error";
status(_) -> "Unknown Status :(".
	       
receive_log() ->
    receive
	{ Fmt, Args} -> io:fwrite(Fmt, Args)
    end,
    receive_log().

get_logger() ->
    Pid = spawn(fun() -> receive_log() end),
    Logger = fun(Fmt, Args) ->
		     Pid ! { Fmt, Args }
	     end,
    Logger.
